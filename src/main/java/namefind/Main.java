package namefind;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Main {

    static List<String> nameLst = new ArrayList<>();

    static int SCORE_LIMIT_TO_DISPLAY = 2;
    static int MATCH_LIMIT_TO_GROUP = 4;

    public static void main(String[] args) {
        try {
            populateLst();
            readNames();
            analyze();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void analyze() {
        Scanner scn = new Scanner(System.in);
        String input = "";

        while (!input.equals("exit")) {
            System.out.println("Enter char to analyze!");
            input = scn.nextLine();
            findGroup(input, MATCH_LIMIT_TO_GROUP, SCORE_LIMIT_TO_DISPLAY);
        }

    }

    public static void findGroup(String letter, int subStrLimit, int highMatch) {
        Map<String, Integer> nameMap = new HashMap<>();
        for (String name : nameLst) {

            if (name.startsWith(letter)) {
                String key = name.substring(0, subStrLimit);
                if (nameMap.get(key) == null) {
                    nameMap.put(key, 1);
                } else {
                    Integer val = nameMap.get(key) + 1;
                    nameMap.put(key, val);
                }
            }
        }

        Map<String, Integer> sortedNameMap = sortByValues(nameMap);

        for (Map.Entry<String, Integer> entry : sortedNameMap.entrySet()) {
            if (entry.getValue() >= highMatch) {

                System.out.print(entry.getKey() + ":" + entry.getValue() + " ");
                for (String name : nameLst) {
                    name = name.substring(0, name.indexOf("="));
                    if (name.startsWith(entry.getKey())) {
                        System.out.print(name);
                    }
                }
                System.out.println("");

            }

        }
    }

    public static void readNames() throws FileNotFoundException {
        if (nameLst.isEmpty()) {
            try (Scanner scnr = new Scanner(new FileInputStream("names.txt"))) {
                while (scnr.hasNextLine()) {
                    nameLst.add(scnr.nextLine());
                }
            }
        }

    }

    public static void writeNameToFile() throws IOException {
        PrintWriter pw = new PrintWriter(new FileWriter("names.txt"));
        for (String name : nameLst) {
            //System.out.println(name);
            pw.println(name);
        }
        pw.close();
    }

    public static void populateLst() throws IOException {
        File f = new File("names.txt");
        if (!f.exists()) {
            for (char ch = 'a'; ch <= 'z'; ch++) {
                if (ch != 'q' && ch != 'w' && ch != 'x') {
                    fetchNames("http://www.indianhindunames.com/indian-hindu-boy-name-" + ch + ".htm");
                }
            }
            writeNameToFile();
        }
    }

    public static void fetchNames(String url) {
        System.out.println(url);
        Document doc;
        try {
            doc = Jsoup.connect(url).get();
            doc.select("br").append("|");
            Elements eLst = doc.select("p");
            for (Element e : eLst) {
                String line = e.text();
                String arr[] = line.split("\\|");
                for (String ele : arr) {
                    if (ele.contains("=")) {
                        nameLst.add(ele.trim().toLowerCase());
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static <K extends Comparable, V extends Comparable> Map<K, V> sortByValues(Map<K, V> map) {
        List<Map.Entry<K, V>> entries = new LinkedList<Map.Entry<K, V>>(map.entrySet());

        Collections.sort(entries, new Comparator<Map.Entry<K, V>>() {

            @Override
            public int compare(Entry<K, V> o1, Entry<K, V> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });

        //LinkedHashMap will keep the keys in the order they are inserted
        //which is currently sorted on natural ordering
        Map<K, V> sortedMap = new LinkedHashMap<K, V>();

        for (Map.Entry<K, V> entry : entries) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }

}
